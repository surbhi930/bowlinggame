package com.target.app.service;

import com.target.app.beans.BowlingGame;
import static com.target.app.constants.GameConstants.TOTAL_FRAME;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import static com.target.app.constants.GameConstants.MAX_RANGE;
import static com.target.app.constants.GameConstants.PINS;
import static com.target.app.constants.GameConstants.FINAL_SET;
import static com.target.app.constants.GameConstants.ERROR_MSG;

@Service
public class GameService {
	List<BowlingGame> games = new ArrayList<>();

	public int startGame(int playersCount, String[] playersName) {

		int gameId = (int) (Math.random() * 100);
		int laneNo = (int) (Math.random() * 4) + 1;
		BowlingGame game = new BowlingGame(gameId, laneNo, playersCount, playersName);
		games.add(game);
		for (int i = 0; i < playersCount; i++) {
			roll(i, game);
		}
		return gameId;
	}

	private void roll(int player, BowlingGame game) {
		int pins[] = new int[TOTAL_FRAME];
		Arrays.fill(pins, 0);
		int maxPins = 0;

		for (int i = 0; i < TOTAL_FRAME; i++) {
			if (i == TOTAL_FRAME - 1) {
				if (pins[i - 1] + pins[i - 2] == PINS) {
					pins[i] = (int) (Math.random() * FINAL_SET);
				}
			} else if (i % 2 == 1) {
				maxPins = PINS - pins[i - 1];
				pins[i] = (int) (Math.random() * maxPins);
			} else {
				pins[i] = (int) (Math.random() * MAX_RANGE);
			}
			game.roll(pins[i], player);
		}
	}

	public String getScoreBoard(int id) {
		StringBuilder scoreBoard = new StringBuilder("");
		boolean found = false;
		BowlingGame game1 = null;
		for (BowlingGame game : games) {
			if (game.getGameId() == id) {
				game1 = game;
				found = true;
				break;
			}
		}
		if (!found) {
			return ERROR_MSG;
		}
		scoreBoard.append("Game Id ").append(game1.getGameId())
		.append("\tAllocated Lane ").append(game1.getLaneNo());
		for (int i = 0; i < game1.getNoOfPlayers(); i++) {
			scoreBoard.append("\nScore ").append(game1.getPlayersName()[i]);
			scoreBoard.append(game1.getScoreCard(i));
		}
		scoreBoard.append("\nTotal Strike = ").append(game1.totalStrike())
				.append("\nTotal Spare = ").append(game1.totalSpare())
				.append("\nMissed Strike = ").append(game1.missedStrike());

		int highestScore = 0;
		String winner = null;
		for (int i = 0; i < game1.getNoOfPlayers(); i++) {
			if (game1.getTotalScore(i) > highestScore) {
				highestScore = game1.getTotalScore(i);
				winner = game1.getPlayersName()[i];
			}
		}
		scoreBoard.append("\nWinner= ").append(winner);
		return scoreBoard.toString();
	}
}