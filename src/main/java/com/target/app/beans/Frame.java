package com.target.app.beans;

import com.target.app.interfaces.BowlingGameInterface;

public class Frame implements BowlingGameInterface {
	private String playerName;
	private int[] frame = new int[TOTAL_FRAME];
	private int score;
	private int currentRoll = 0;

	protected String getPlayerName() {
		return playerName;
	}

	protected void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	protected int[] getFrame() {
		return frame;
	}

	protected int getScore() {
		return score;
	}

	protected int getCurrentRoll() {
		return currentRoll;
	}

	@Override
	public void roll(int pins) {
		if (currentRoll >= TOTAL_FRAME)
			return;
		frame[currentRoll++] = pins;
	}

	@Override
	public int score() {
		score=0;
		for (int i = 0; i < TOTAL_FRAME; i++) {
			if(i==TOTAL_FRAME-1) {
				score += frame[i];
			}
			else if (isStrike(i)) {
				score += frame[i++] + STRIKE_BONUS;
			}
			else if (isSpare(i)) {
				score += frame[i] + frame[++i] + SPARE_BONUS;
			} else {
				score += frame[i];
			}
		}
		return score;
	}

	protected boolean isSpare(int chance) {
		if ((frame[chance] + frame[chance + 1] == PINS) && chance % 2 == 0)
			return true;
		return false;
	}

	protected boolean isStrike(int chance) {
		if (chance % 2 == 0 && frame[chance] == PINS)
			return true;
		return false;
	}

	protected int currentStrike() {
		int currentStrike = 0;
		for (int i = 0; i < TOTAL_FRAME; i += 2) {
			if (frame[i] == PINS) {
				currentStrike++;
			}
		}
		return currentStrike;
	}

	protected int currentSpare() {
		int currentSpare = 0;
		for (int i = 0; i < TOTAL_FRAME-1; i += 2) {
			if (frame[i] + frame[i + 1] == PINS) {
				currentSpare++;
			}
		}
		return currentSpare;
	}

	protected int missedStrike() {
		int missedStrike = 0;
		for (int i = 0; i < TOTAL_FRAME; i += 2) {
			if (frame[i] == MISSED_STRIKE) {
				missedStrike++;
			}
		}
		return missedStrike;
	}
}