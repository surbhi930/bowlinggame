package com.target.app.interfaces;

public interface BowlingGameInterface {
	
	public static final int MAX_PLAYERS=3;
	public static final int PINS=10;
	public static final int TOTAL_FRAME=21;
	public static final int MISSED_STRIKE=9;
	public static final int STRIKE_BONUS=10;
	public static final int SPARE_BONUS=5;
	
	public void roll(int pins);
	public int score();
}